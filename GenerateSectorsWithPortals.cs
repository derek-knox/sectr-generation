// GenerateSectorsWithPortals.cs
// Unity Editor extension that generates Sectors with automatically configured Portals (Procedural Worlds Sectr asset)
// Via Derek Knox (Twitter: @derekknox)

using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;
using System.IO;

public class GenerateSectorsWithPortals : ScriptableWizard
{
    public int SectorVolumeWidth = 32;
    public int SectorVolumeHeight = 18;
    public int SectorVolumeDepth = 5;
    public GameObject SectorTemplate;
    public int GridColumnCount = 20;
    public int GridRowCount = 12;
    public string directory = "Procedural Worlds Generated";
    private IOrderedEnumerable<Transform> orderedSectorTransforms;
    private GameObject sectors;
    private GameObject portals;
    private GameObject sectorPrefab;
    private GameObject portalPrefab;
    private string generatedSectorLocalPath;
    private string generatedPortalLocalPath;
    private string generatedSectorBoundsLocalPath;

    private enum PrefabTypes
    {
        Sector,
        Portal
    }

    [MenuItem("Edit/Generate Sectors with Portals")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard("Generate Sectors with Portals", typeof(GenerateSectorsWithPortals), "Generate");
    }

    void OnEnable()
    {
        RefreshContainerCache();
        RefreshHelpString();
    }

    void OnWizardCreate()
    {
        // Update caches and temporary prefabs
        Prepare();

        // Generate the sector portals
        Regenerate();

        // Clean up the temporary prefabs
        Clean();
    }

    void Prepare()
    {
        PreparePaths();
        PrepareSectors();
        PreparePortals();

        RefreshContainerCache();
    }

    void RefreshContainerCache()
    {
        sectors = GameObject.Find("Sectors");
        portals = GameObject.Find("Portals");

        if (sectors != null)
        {
            orderedSectorTransforms = sectors.transform.Cast<Transform>().OrderBy(t => t.name);
        }
    }

    void RefreshHelpString()
    {
        if (orderedSectorTransforms != null)
        {
            helpString = $"Prepared to regenerate all portals for {orderedSectorTransforms.Count()} found sectors.";
        }
        else
        {
            helpString = "No sectors found. Use the settings below to generate them and their portals.";
        }
    }

    void PreparePaths()
    {
        string targetDirectory = Path.Combine("Assets", directory);
        bool hasDirectory = AssetDatabase.IsValidFolder(targetDirectory);
        string guid = hasDirectory ? AssetDatabase.AssetPathToGUID(targetDirectory) : AssetDatabase.CreateFolder("Assets", directory);
        string path = AssetDatabase.GUIDToAssetPath(guid);

        generatedSectorLocalPath = Path.Combine(path, "Sector.prefab");
        generatedPortalLocalPath = Path.Combine(path, "Portal.prefab");
        generatedSectorBoundsLocalPath = Path.Combine(path, "SectorBounds.asset");
    }

    void PrepareSectors()
    {
        // Sectors (if not already created)
        if (sectors == null)
        {
            sectors = new GameObject("Sectors");
            sectorPrefab = BuildPrefab(PrefabTypes.Sector, generatedSectorLocalPath);
            GenerateSectors();
        }

        sectors.isStatic = true;
    }

    void PreparePortals()
    {
        // Portals
        if (portals == null)
        {
            portals = new GameObject("Portals");
        }
        else
        {
            // Clear existing portals
            foreach (Transform child in portals.transform.Cast<Transform>().ToList())
            {
                GameObject.DestroyImmediate(child.gameObject);
            }
        }

        // Build portal prefab regardless of portals existence to prep regeneration
        portalPrefab = BuildPrefab(PrefabTypes.Portal, generatedPortalLocalPath);
    }

    GameObject BuildPrefab(PrefabTypes type, string path)
    {
        // Cache (Sector vs. Portal)
        bool isSector = PrefabTypes.Sector == type;

        // Create prefab template
        GameObject prefab = GameObject.CreatePrimitive(isSector ? PrimitiveType.Cube : PrimitiveType.Quad);
        Collider collider = prefab.GetComponent<Collider>();
        DestroyImmediate(collider);
        MeshRenderer meshRenderer = prefab.GetComponent<MeshRenderer>();
        meshRenderer.shadowCastingMode = ShadowCastingMode.Off;

        // Sector vs. Portal
        if (isSector)
        {
            prefab.AddComponent<SECTR_Sector>();

            // Update bounds to match user settings
            BuildSectorVolume(prefab.GetComponent<MeshFilter>());

            // Conditionally match template structure
            if (SectorTemplate != null)
            {
                // Walk children in reverse to ensure no skipping
                GameObject template = Instantiate(SectorTemplate);
                for (int i = template.transform.childCount - 1; i >= 0; --i)
                {
                    // Reparent child
                    Transform child = template.transform.GetChild(i);
                    child.parent = prefab.transform;

                    // Reset original order of child due to reverse walk
                    child.SetSiblingIndex(0);
                }
                DestroyImmediate(template);
            }
        }
        else
        {
            prefab.AddComponent<SECTR_Portal>();
        }

        // Make sure the file name is unique, in case an existing Prefab has the same name.
        path = AssetDatabase.GenerateUniqueAssetPath(path);

        // Create the new Prefab asset
        GameObject targetPrefab = PrefabUtility.SaveAsPrefabAssetAndConnect(prefab, path, InteractionMode.UserAction);

        // Clean the Hierarchy 
        DestroyImmediate(prefab);

        // Return built prefab
        return targetPrefab;
    }

    void BuildSectorVolume(MeshFilter meshFilter)
    {
        // Clone mesh
        Mesh original = meshFilter.sharedMesh;
        Mesh clone = Instantiate(original);
        meshFilter.mesh = clone;

        // Modify bounds
        Vector3[] originalVertices = meshFilter.sharedMesh.vertices;
        Vector3[] targetVertices = new Vector3[originalVertices.Length];
        for (int i = 0; i < targetVertices.Length; i++)
        {
            Vector3 vertex = originalVertices[i];
            vertex.x *= SectorVolumeWidth;
            vertex.y *= SectorVolumeHeight;
            vertex.z *= SectorVolumeDepth;
            targetVertices[i] = vertex;
        }

        // Reapply modification
        meshFilter.sharedMesh.vertices = targetVertices;
        meshFilter.sharedMesh.RecalculateNormals();
        meshFilter.sharedMesh.RecalculateBounds();

        // Persist clone mesh for SceneView use while ensuring a clone where the original Unity primitive is left unmodified
        AssetDatabase.CreateAsset(clone, generatedSectorBoundsLocalPath);
        AssetDatabase.SaveAssets();
    }

    void Regenerate()
    {
        for (int i = 0; i < GridRowCount; i++)
        {
            for (int j = 0; j < GridColumnCount; j++)
            {
                int index = i * GridColumnCount + j;
                Transform sectorTransform = orderedSectorTransforms.ElementAt(index);
                if (sectorTransform)
                {
                    bool isAtLeftEdge = j == 0;
                    bool isAtRightEdge = j + 1 == GridColumnCount;
                    bool isAtBottomEdge = i + 1 == GridRowCount;

                    Transform right = isAtRightEdge ? null : orderedSectorTransforms.ElementAt(index + 1);
                    Transform diagonalForward = isAtRightEdge || isAtBottomEdge ? null : orderedSectorTransforms.ElementAt(index + GridColumnCount + 1);
                    Transform down = isAtBottomEdge ? null : orderedSectorTransforms.ElementAt(index + GridColumnCount);
                    Transform diagonalBack = isAtLeftEdge || isAtBottomEdge ? null : orderedSectorTransforms.ElementAt(index + GridColumnCount - 1);
                    GeneratePortals(sectorTransform, right, diagonalForward, down, diagonalBack);
                }
            }
        }
    }

    void GenerateSectors()
    {
        for (int i = 0; i < GridRowCount; i++)
        {
            for (int j = 0; j < GridColumnCount; j++)
            {
                int index = i * GridColumnCount + j;
                GenerateSector(index + 1, i, j);
            }
        }
    }

    void GenerateSector(int id, int i, int j)
    {
        GameObject sectorGameObject = Instantiate(sectorPrefab, Vector3.zero, Quaternion.identity);

        NameSector(sectorGameObject, id);
        sectorGameObject.isStatic = true;
        sectorGameObject.transform.parent = sectors.transform;
        sectorGameObject.transform.position += new Vector3(SectorVolumeWidth * j, -SectorVolumeHeight * i, 0);
    }

    void NameSector(GameObject sectorGameObject, int id)
    {
        string prefix = " ";
        if (id < 10) { prefix += "00"; }
        else if (id < 100) { prefix += "0"; }
        sectorGameObject.name = $"Sector{prefix}{id}";
    }

    void GeneratePortals(Transform sectorTransform, Transform right, Transform diagonalForward, Transform down, Transform diagonalBack)
    {
        SECTR_Sector sector = sectorTransform.GetComponent<SECTR_Sector>();
        Vector3 offsetRight = Vector3.right * (SectorVolumeWidth / 2);
        Vector3 offsetDown = -Vector3.up * (SectorVolumeHeight / 2);

        if (right != null) { GeneratePortal(sector, right.GetComponent<SECTR_Sector>(), offsetRight); }
        if (diagonalForward != null) { GeneratePortal(sector, diagonalForward.GetComponent<SECTR_Sector>(), offsetRight + offsetDown); }
        if (down != null) { GeneratePortal(sector, down.GetComponent<SECTR_Sector>(), offsetDown); }
        if (diagonalBack != null) { GeneratePortal(sector, diagonalBack.GetComponent<SECTR_Sector>(), -offsetRight + offsetDown); }
    }

    void GeneratePortal(SECTR_Sector front, SECTR_Sector back, Vector3 offset)
    {
        GameObject portalGameObject = Instantiate(portalPrefab, Vector3.zero, Quaternion.identity);
        SECTR_Portal portal = portalGameObject.GetComponent<SECTR_Portal>();
        portal.FrontSector = front;
        portal.BackSector = back;

        portalGameObject.transform.parent = portals.transform;
        portalGameObject.name = "Portal - " + front.transform.name + " - " + back.transform.name;
        portalGameObject.transform.position = front.transform.position + offset;
    }

    void Clean()
    {
        AssetDatabase.DeleteAsset(generatedSectorLocalPath);
        AssetDatabase.DeleteAsset(generatedPortalLocalPath);
    }
}